package ru.s1x6.ration.db.entity

import javax.persistence.*

@Entity
@Table(name = "products")
class ProductEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var name: String,
        var calories: Double, // per 100g
        var carbohydrates: Double, // per 100g
        var proteins: Double, //per 100g
        var fats: Double, // per 100g
        var userId: Long?, // the user who added this product
        var portionSize: Double // grams per portion
)