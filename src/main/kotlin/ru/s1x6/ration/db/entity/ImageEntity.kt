package ru.s1x6.ration.db.entity

import javax.persistence.*

@Entity
@Table(name = "images")
class ImageEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var name: String,
        @Lob
        var image: ByteArray
)