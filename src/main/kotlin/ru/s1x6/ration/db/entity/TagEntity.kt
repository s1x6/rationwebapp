package ru.s1x6.ration.db.entity

import javax.persistence.*

@Entity
@Table(name = "tags")
class TagEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var name: String
)