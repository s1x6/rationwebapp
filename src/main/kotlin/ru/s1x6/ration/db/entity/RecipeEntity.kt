package ru.s1x6.ration.db.entity

import javax.persistence.*

@Entity
@Table(name = "recipes")
class RecipeEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var name: String,
        var description: String,
        var portionSize: Double,
        var userId: Long,
        var fats: Double,
        var proteins: Double,
        var carbohydrates: Double,
        var calories: Double,
        var imageName: String,
        @ElementCollection
        var steps: List<Pair<Int, String>>,
        @ManyToMany
        var tags: List<TagEntity>
)