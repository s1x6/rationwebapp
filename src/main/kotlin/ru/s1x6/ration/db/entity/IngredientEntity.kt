package ru.s1x6.ration.db.entity

import javax.persistence.*

@Entity
@Table(name = "ingredients")
class IngredientEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var recipeId: Long,
        var productId: Long,
        var amount: Double // grams
)