package ru.s1x6.ration.db.entity

import javax.persistence.*

@Entity
@Table(name = "users")
class UserEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,
        var login: String,
        var passwordHash: String,
        var email: String,
        var name: String,
        var lastIp: String? = null,
        var role: String = "ROLE_CLIENT"
)