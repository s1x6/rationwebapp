package ru.s1x6.ration.db.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.s1x6.ration.db.entity.ImageEntity

@Repository
interface ImageRepository: JpaRepository<ImageEntity, Long> {
    fun getByName(name: String): ImageEntity?
}