package ru.s1x6.ration.db.repository

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.s1x6.ration.db.entity.RecipeEntity

@Repository
interface RecipeRepository: JpaRepository<RecipeEntity, Long> {
    fun findAllByOrderByIdDesc(pageable: Pageable): List<RecipeEntity>
}