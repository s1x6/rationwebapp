package ru.s1x6.ration.db.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.s1x6.ration.db.entity.UserEntity

@Repository
interface UserRepository: JpaRepository<UserEntity, Long> {
    fun getByEmailAndLogin(email: String, login: String): List<UserEntity>
    fun getByLogin(login: String): UserEntity?
}