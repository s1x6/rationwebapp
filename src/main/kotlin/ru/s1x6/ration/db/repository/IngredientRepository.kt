package ru.s1x6.ration.db.repository

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.s1x6.ration.db.entity.IngredientEntity

@Repository
interface IngredientRepository: JpaRepository<IngredientEntity, Long> {

    fun getAllByProductId(productId: Long, pageable: Pageable): List<IngredientEntity>
    fun getAllByRecipeId(productId: Long): List<IngredientEntity>
}