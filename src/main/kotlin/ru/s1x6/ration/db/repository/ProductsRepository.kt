package ru.s1x6.ration.db.repository

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.s1x6.ration.db.entity.ProductEntity

@Repository
interface ProductsRepository: JpaRepository<ProductEntity, Long>  {
    fun getAllByUserIdIsNullOrUserId(userId: Long, pageable: Pageable): List<ProductEntity>
    fun getAllByNameContainingIgnoreCase(name: String, pageable: Pageable): List<ProductEntity>
    fun getAllByIdIn(ids: Collection<Long>): List<ProductEntity>
}