package ru.s1x6.ration.service

import ru.s1x6.ration.model.FullProductInfo
import ru.s1x6.ration.model.Result

interface ProductService {
    fun getAllSystemProducts(page: Int, limit: Int): Result
    fun getAllProductsForUser(page: Int, limit: Int, userId: Long): Result
    fun getSize(): Result
    fun getInfo(id: Long): Result
    fun saveProduct(fullProductInfo: FullProductInfo, userId: Long): Result
    fun getSearch(q: String): Result
}