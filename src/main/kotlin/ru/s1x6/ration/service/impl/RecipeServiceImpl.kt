package ru.s1x6.ration.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.s1x6.ration.db.entity.ImageEntity
import ru.s1x6.ration.db.entity.IngredientEntity
import ru.s1x6.ration.db.entity.ProductEntity
import ru.s1x6.ration.db.entity.RecipeEntity
import ru.s1x6.ration.db.repository.*
import ru.s1x6.ration.model.*
import ru.s1x6.ration.service.PortionCounter
import ru.s1x6.ration.service.RecipeService


@Service
class RecipeServiceImpl : RecipeService {

    @Autowired
    private lateinit var tagRepository: TagRepository

    @Autowired
    private lateinit var recipeRepository: RecipeRepository

    @Autowired
    private lateinit var productRepository: ProductsRepository

    @Autowired
    private lateinit var ingredientRepository: IngredientRepository

    @Autowired
    private lateinit var portionCounter: PortionCounter

    @Autowired
    private lateinit var imageRepository: ImageRepository

    override fun getTags() = SuccessResult(tagRepository.findAll())

    override fun getRandomRecipes(amount: Int): Result {
        val qty: Long = recipeRepository.count()
        val recipes = mutableListOf<RecipeEntity>()
        repeat(amount) {
            val idx = (Math.random() * qty).toInt()
            val page = recipeRepository.findAll(PageRequest.of(idx, 1))
            if (page.hasContent()) {
                recipes.add(page.content[0])
            }
        }
        return SuccessResult(recipes)
    }

    override fun getLatestRecipes(): Result {
        return SuccessResult(
                recipeRepository.findAllByOrderByIdDesc(PageRequest.of(0, 10))
        )
    }

    override fun getFullRecipe(id: Long, currentUserId: Long): Result {
        val recipe = recipeRepository.getOne(id)
        val ingredients = ingredientRepository.getAllByRecipeId(id)
        val products = productRepository.getAllByIdIn(ingredients.map { it.productId })
        val defaultNutrients = NutrientsInfo(recipe.calories, recipe.carbohydrates, recipe.proteins, recipe.fats)
        val fullProductInfo = FullRecipeInfo(
                id, recipe.name, recipe.description, recipe.portionSize, recipe.userId,
                defaultNutrients,
                countPortionNutrients(defaultNutrients, recipe.portionSize),
                countTotalNutrients(products, ingredients),
                countTotalWeight(ingredients),
                recipe.imageName,
                recipe.tags, recipe.steps.map { RecipeStepInfo(it.first, it.second) },
                products.map { pe ->
                    FullIngredientInfo(pe.id, pe.name, pe.fats, pe.proteins,
                            pe.carbohydrates, pe.calories,
                            ingredients.find { pe.id == it.productId }?.amount ?: 0.0)
                },
                recipe.userId == currentUserId
        )
        return SuccessResult(fullProductInfo)
    }

    private fun countTotalNutrients(products: List<ProductEntity>, ingredients: List<IngredientEntity>): NutrientsInfo {
        var cl = 0.0
        var cb = 0.0
        var p = 0.0
        var f = 0.0
        repeat(products.size) { i ->
            cl += products[i].calories * ingredients[i].amount / 100
            cb += products[i].carbohydrates * ingredients[i].amount / 100
            f += products[i].fats * ingredients[i].amount / 100
            p += products[i].proteins * ingredients[i].amount / 100
        }
        return NutrientsInfo(
                cl, cb, p, f
        )
    }

    private fun countPortionNutrients(nutrients: NutrientsInfo, portionSize: Double) = NutrientsInfo(
            nutrients.calories / 100 * portionSize,
            nutrients.carbohydrates / 100 * portionSize,
            nutrients.proteins / 100 * portionSize,
            nutrients.fats / 100 * portionSize
    )

    private fun countTotalWeight(ingredients: List<IngredientEntity>): Double = ingredients.sumOf { it.amount }

    override fun saveImage(img: ByteArray, name: String): Result {
        if (imageRepository.getByName(name) == null) {
            imageRepository.save(
                    ImageEntity(
                            0,
                            name,
                            img
                    )
            )
            return SuccessResult()
        }
        return ErrorResult("Изображение с таким именем уже существует")
    }

    // this one is transactional because it pulls data from LOB lazily in another request
    @Transactional
    override fun getImage(name: String): ByteArray? {
        val i = imageRepository.getByName(name)
        return i?.image
    }

    override fun saveRecipe(body: CreateRecipeInfo, userId: Long): Result {
        val nutrients = calculateNutrients(body.ingredients)
        val entity = RecipeEntity(
                0, body.name, body.description, body.portionSize, userId,
                nutrients.fats, nutrients.proteins, nutrients.carbohydrates,
                nutrients.calories, body.imageName, body.steps.map { it.num to it.text }, body.tags
        )
        // TODO make transactional?
        val savedRecipe = recipeRepository.save(entity)
        val ingredientEntities = body.ingredients.map { i -> IngredientEntity(0, savedRecipe.id, i.productId, i.amount) }
        ingredientRepository.saveAll(ingredientEntities)
        return SuccessResult(savedRecipe)
    }

    fun calculateNutrients(products: List<IngredientInfo>): NutrientsInfo {
        val portionInfos = products.map {
            portionCounter.getProductPortionInfo(it.productId, it.amount)
        }
        val weight = products.sumOf { it.amount }
        val clPer100 = portionInfos.sumOf { it.calories } / weight * 100
        val cbPer100 = portionInfos.sumOf { it.carbohydrates } / weight * 100
        val pPer100 = portionInfos.sumOf { it.proteins } / weight * 100
        val fPer100 = portionInfos.sumOf { it.fats } / weight * 100
        return NutrientsInfo(
                clPer100, cbPer100, pPer100, fPer100
        )
    }

    override fun getNutrients(products: List<IngredientInfo>): Result {
        return SuccessResult(calculateNutrients(products))
    }
}