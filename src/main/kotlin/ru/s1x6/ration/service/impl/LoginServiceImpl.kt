package ru.s1x6.ration.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Service
import ru.s1x6.ration.db.repository.UserRepository
import ru.s1x6.ration.model.*
import ru.s1x6.ration.security.JwtTokenProvider
import ru.s1x6.ration.security.model.Role
import ru.s1x6.ration.service.LoginService
import javax.servlet.http.HttpServletRequest

@Service
class LoginServiceImpl: LoginService {
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var authenticationManager: AuthenticationManager
    @Autowired
    private lateinit var jwtTokenProvider: JwtTokenProvider

    override fun signUp(info: SignUpInfo): Result {
        return if (isUserDataUnique(info)) {
            val userEntity = info.toUserEntity(true)
            userRepository.save(userEntity)
            SuccessResult()
        } else {
            ErrorResult("Пользователь с таким именем или email уже зарегистриован")
        }
    }

    override fun tryLogin(loginInfo: LoginInfo, ip: String, request: HttpServletRequest): Result {
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(loginInfo.login, loginInfo.password))
        val user = userRepository.getByLogin(loginInfo.login)!!
        val authToken = jwtTokenProvider.createToken(loginInfo.login,
                listOf(Role.valueOf(user.role)))
        user.lastIp = request.remoteAddr
        userRepository.save(user)
        return SuccessResult(authToken)
    }

    private fun isUserDataUnique(userData: SignUpInfo): Boolean {
        return userRepository.getByEmailAndLogin(userData.email, userData.login).isEmpty()
    }
}
