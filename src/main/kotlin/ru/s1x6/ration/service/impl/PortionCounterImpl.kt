package ru.s1x6.ration.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.s1x6.ration.db.repository.ProductsRepository
import ru.s1x6.ration.model.ProductPortionInfo
import ru.s1x6.ration.service.PortionCounter

@Service
class PortionCounterImpl : PortionCounter {

    @Autowired
    private lateinit var productsRepository: ProductsRepository

    override fun getProductPortionInfo(productId: Long, portionSize: Double): ProductPortionInfo {
        val product = productsRepository.getOne(productId)
        return ProductPortionInfo(
                product.proteins / 100 * portionSize,
                product.carbohydrates / 100 * portionSize,
                product.fats / 100 * portionSize,
                portionSize,
                product.calories / 100.0 * portionSize,
        )
    }
}