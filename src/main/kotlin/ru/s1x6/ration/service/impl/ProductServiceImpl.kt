package ru.s1x6.ration.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import ru.s1x6.ration.db.repository.ProductsRepository
import ru.s1x6.ration.model.*
import ru.s1x6.ration.service.PortionCounter
import ru.s1x6.ration.service.ProductService

@Service
class ProductServiceImpl : ProductService {

    @Autowired
    private lateinit var productsRepository: ProductsRepository

    @Autowired
    private lateinit var portionCounter: PortionCounter

    override fun getAllSystemProducts(page: Int, limit: Int): Result {
        return SuccessResult()
    }

    // returns products added by this user and added by system
    override fun getAllProductsForUser(page: Int, limit: Int, userId: Long): Result {
        val pageable = PageRequest.of(page, limit)
        val userProducts = productsRepository.getAllByUserIdIsNullOrUserId(userId, pageable)
        return SuccessResult(userProducts.map { it.toProductInfo() })
    }

    override fun getSize(): Result {
        return SuccessResult(productsRepository.count())
    }

    override fun getInfo(id: Long): Result {
        val product = productsRepository.getOne(id)
        val portionInfo = portionCounter.getProductPortionInfo(id, product.portionSize)
        return SuccessResult(
                FullProductInfo(
                        product.id,
                        product.name,
                        product.calories,
                        product.carbohydrates,
                        product.proteins,
                        product.fats,
                        product.userId == null,
                        portionInfo
                )
        )
    }

    override fun getSearch(q: String): Result {
        val pageable = PageRequest.of(0, 10)
        val result = productsRepository.getAllByNameContainingIgnoreCase(q, pageable)
                .map { it.toProductInfo() }
        return SuccessResult(result)
    }

    override fun saveProduct(fullProductInfo: FullProductInfo, userId: Long): Result {
        val savedProductOpt = productsRepository.findById(fullProductInfo.id)
        if (savedProductOpt.isPresent) {
            val savedProduct = savedProductOpt.get()
            if (savedProduct.userId != userId) {
                return ErrorResult("Вы не можете редактировать продукты, добавленные не Вами!")
            }
        }
        val entity = fullProductInfo.toEntity()
        entity.userId = userId
        val id = productsRepository.save(entity).id
        return getInfo(id)
    }
}
