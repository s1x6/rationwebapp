package ru.s1x6.ration.service

import ru.s1x6.ration.model.LoginInfo
import ru.s1x6.ration.model.Result
import ru.s1x6.ration.model.SignUpInfo
import javax.servlet.http.HttpServletRequest

interface LoginService {
    fun signUp(info: SignUpInfo): Result
    fun tryLogin(loginInfo: LoginInfo, ip: String, request: HttpServletRequest): Result
}