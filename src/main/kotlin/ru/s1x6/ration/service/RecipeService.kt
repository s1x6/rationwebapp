package ru.s1x6.ration.service

import ru.s1x6.ration.model.CreateRecipeInfo
import ru.s1x6.ration.model.IngredientInfo
import ru.s1x6.ration.model.Result

interface RecipeService {
    fun getTags(): Result
    fun getRandomRecipes(amount: Int): Result
    fun saveRecipe(body: CreateRecipeInfo, userId: Long): Result
    fun getNutrients(products: List<IngredientInfo>): Result
    fun getLatestRecipes(): Result
    fun saveImage(img: ByteArray, name: String): Result
    fun getImage(name: String): ByteArray?
    fun getFullRecipe(id: Long, currentUserId: Long): Result
}