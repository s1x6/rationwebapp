package ru.s1x6.ration.service

import ru.s1x6.ration.model.ProductPortionInfo

interface PortionCounter {
    fun getProductPortionInfo(productId: Long, portionSize: Double): ProductPortionInfo
}