package ru.s1x6.ration.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import ru.s1x6.ration.security.exception.CustomException
import ru.s1x6.ration.security.model.Role
import java.util.*
import java.util.Base64.getEncoder
import java.util.stream.Collectors
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest


@Component
class JwtTokenProvider {
    @Value("\${security.jwt.token.secret-key}")
    private var secretKey: String? = null
    @Value("\${security.jwt.token.expire-length}")
    private var validityInMilliseconds: Long? = null // 1h
    @Autowired
    private lateinit var userDetails: ru.s1x6.ration.security.UserDetails

    @PostConstruct
    protected fun init() {
        secretKey = getEncoder().encodeToString(secretKey!!.toByteArray())
    }

    fun createToken(username: String?, roles: List<Role>): String {
        val claims: Claims = Jwts.claims().setSubject(username)
        claims["auth"] = roles.stream().map { s: Role -> SimpleGrantedAuthority(s.authority) }
                .filter { obj: SimpleGrantedAuthority? -> Objects.nonNull(obj) }.collect(Collectors.toList())
        val now = Date()
        val validity = Date(now.time + validityInMilliseconds!!)
        return Jwts.builder() //
                .setClaims(claims) //
                .setIssuedAt(now) //
                .setExpiration(validity) //
                .signWith(SignatureAlgorithm.HS256, secretKey) //
                .compact()
    }

    fun getAuthentication(token: String?): Authentication {
        val userDetails: UserDetails = userDetails.loadUserByUsername(getUsername(token))
        return UsernamePasswordAuthenticationToken(userDetails, "", userDetails.authorities)
    }

    fun getUsername(token: String?): String {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).body.subject
    }

    fun resolveToken(req: HttpServletRequest): String? {
        val bearerToken = req.getHeader("Authorization")
        return if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            bearerToken.substring(7)
        } else null
    }

    fun validateToken(token: String?): Boolean {
        return try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token)
            true
        } catch (e: JwtException) {
            throw CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR)
        } catch (e: IllegalArgumentException) {
            throw CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}