package ru.s1x6.ration.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import ru.s1x6.ration.db.entity.UserEntity
import ru.s1x6.ration.db.repository.UserRepository
import ru.s1x6.ration.security.model.Role


@Service
class UserDetails : UserDetailsService {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): ExtendedUserDetails {
        val user: UserEntity = userRepository.getByLogin(username)
                ?: throw UsernameNotFoundException("User '$username' not found")
        return object : ExtendedUserDetails {
            override val id = user.id
            override fun getAuthorities() = mutableListOf(Role.valueOf(user.role))
            override fun getPassword() = user.passwordHash
            override fun getUsername() = user.login
            override fun isAccountNonExpired() = true
            override fun isAccountNonLocked() = true
            override fun isCredentialsNonExpired() = true
            override fun isEnabled() = true
        }
    }

    interface ExtendedUserDetails : UserDetails {
        val id: Long
    }
}