package ru.s1x6.ration.security.model

import org.springframework.security.core.GrantedAuthority

enum class Role : GrantedAuthority {
    ROLE_ADMIN, ROLE_CLIENT;

    override fun getAuthority() = name
}