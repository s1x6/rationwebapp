package ru.s1x6.ration.model

import ru.s1x6.ration.db.entity.TagEntity

data class FullRecipeInfo(
        var id: Long,
        var name: String,
        var description: String,
        var portionSize: Double,
        var userId: Long,
        var nutrientsDefault: NutrientsInfo,
        var nutrientsPortion: NutrientsInfo,
        var nutrientsTotal: NutrientsInfo,
        var totalWeight: Double,
        var imageName: String,
        var tags: List<TagEntity>,
        var steps: List<RecipeStepInfo>,
        var ingredients: List<FullIngredientInfo>,
        var isEditable: Boolean
)

data class RecipeStepInfo(var num: Int, var text: String)

data class FullIngredientInfo(
    var productId: Long,
    val name: String,
    var fats: Double,
    var proteins: Double,
    var carbohydrates: Double,
    var calories: Double,
    var amount: Double
)

data class IngredientInfo(
        var productId: Long,
        var name: String,
        var amount: Double
)

data class CreateRecipeInfo(
        var name: String,
        var description: String,
        var portionSize: Double,
        var steps: List<RecipeStepInfo>,
        var imageName: String,
        var tags: MutableList<TagEntity>,
        var ingredients: MutableList<IngredientInfo>
)