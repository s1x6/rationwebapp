package ru.s1x6.ration.model

open class Result(val status: StatusType)
enum class StatusType { OK, ERROR }
data class SuccessResult(val data: Any? = null): Result(StatusType.OK)
data class ErrorResult(val errorText: String): Result(StatusType.ERROR)