package ru.s1x6.ration.model

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import ru.s1x6.ration.db.entity.UserEntity

data class LoginInfo(val login: String, val password: String)
data class SignUpInfo(val email: String, val login: String, val name: String, val password: String) {
    fun toUserEntity(shouldHash: Boolean) = UserEntity(
            0,
            login,
            if (shouldHash) {
                BCryptPasswordEncoder(12).encode(password)
            } else password,
            email,
            name
    )
}