package ru.s1x6.ration.model

data class ProductPortionInfo(
        val proteins: Double,
        val carbohydrates: Double,
        val fats: Double,
        val size: Double,
        val calories: Double
)