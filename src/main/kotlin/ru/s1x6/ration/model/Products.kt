package ru.s1x6.ration.model

import ru.s1x6.ration.db.entity.ProductEntity

data class ProductInfo(
        var id: Long,
        var name: String,
        var calories: Double, // per 100g
        var carbohydrates: Double, // per 100g
        var proteins: Double, //per 100g
        var fats: Double, // per 100g
        var isSystem: Boolean
)

data class NutrientsInfo(
        var calories: Double, // per 100g
        var carbohydrates: Double, // per 100g
        var proteins: Double, //per 100g
        var fats: Double, // per 100g
)

fun ProductEntity.toProductInfo() = ProductInfo(
        this.id,
        this.name,
        this.calories,
        this.carbohydrates,
        this.proteins,
        this.fats,
        this.userId == null
)

data class FullProductInfo(
        var id: Long,
        var name: String,
        var calories: Double, // per 100g
        var carbohydrates: Double, // per 100g
        var proteins: Double, //per 100g
        var fats: Double, // per 100g
        var isSystem: Boolean,
        var portionInfo: ProductPortionInfo
) {
    fun toEntity() = ProductEntity(
            id,
            name,
            calories,
            carbohydrates,
            proteins,
            fats,
            null,
            portionInfo.size
    )
}