package ru.s1x6.ration.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import ru.s1x6.ration.model.FullProductInfo
import ru.s1x6.ration.model.Result
import ru.s1x6.ration.service.ProductService

@RestController
class ProductController : AuthenticatedController() {

    @Autowired
    private lateinit var productService: ProductService

    @GetMapping(value = ["/product"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAllProductsForUser(
            @RequestParam limit: Int,
            @RequestParam page: Int,
            authentication: Authentication
    ): Result {
        return productService.getAllProductsForUser(page, limit, getUserIdByAuthentication(authentication)
                ?: throw RuntimeException("User id not found"))
    }

    @GetMapping(value = ["/product/size"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProductsAmount(): Result {
        return productService.getSize()
    }

    @GetMapping(value = ["/product/{id}"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProductInfo(@PathVariable id: Long): Result {
        return productService.getInfo(id)
    }

    @PostMapping(value = ["/product"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun saveProduct(@RequestBody fullProductInfo: FullProductInfo, authentication: Authentication): Result {
        return productService.saveProduct(fullProductInfo, getUserIdByAuthentication(authentication)
                ?: throw RuntimeException("User id not found"))
    }

    @GetMapping(value = ["/product/search"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProductSearch(@RequestParam q: String): Result {
        return productService.getSearch(q)
    }

}