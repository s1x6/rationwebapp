package ru.s1x6.ration.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.s1x6.ration.model.CreateRecipeInfo
import ru.s1x6.ration.model.Result
import ru.s1x6.ration.service.RecipeService

@RestController
class RecipeController: AuthenticatedController() {

    @Autowired
    private lateinit var recipeService: RecipeService

    @GetMapping(value = ["/recipe/tag"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getTags(): Result {
        return recipeService.getTags()
    }

    @GetMapping(value = ["/recipe/random"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRandomRecipes(): Result {
        return recipeService.getRandomRecipes(3)
    }

    @GetMapping(value = ["/recipe/latest"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getLatestRecipes(): Result {
        return recipeService.getLatestRecipes()
    }

    @GetMapping(value = ["/recipe"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getSearchRecipes(@RequestParam q: String): Result {
        return recipeService.getRandomRecipes(3)
    }

    @GetMapping(value = ["/recipe/{id}"],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFullRecipe(@PathVariable id: Long, authentication: Authentication): Result {
        return recipeService.getFullRecipe(id, getUserIdByAuthentication(authentication)
                ?: throw RuntimeException("No such user"))
    }

    @PostMapping(value = ["/recipe"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun saveRecipe(@RequestBody body: CreateRecipeInfo, authentication: Authentication): Result {
        return recipeService.saveRecipe(body, getUserIdByAuthentication(authentication)
                ?: throw RuntimeException("No such user"))
    }

    @PostMapping(value = ["/recipe/image"],
            consumes = [MediaType.MULTIPART_FORM_DATA_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun saveRecipeImage(@RequestParam file: MultipartFile, @RequestParam name: String): Result {
        return recipeService.saveImage(file.bytes, name)
    }

    @GetMapping(value = ["/recipe/images"],
           produces = [MediaType.IMAGE_JPEG_VALUE])
    fun getImage(@RequestParam name: String): ByteArray? {
        return recipeService.getImage(name)
    }
}