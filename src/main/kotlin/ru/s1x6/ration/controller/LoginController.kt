package ru.s1x6.ration.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.s1x6.ration.model.LoginInfo
import ru.s1x6.ration.model.Result
import ru.s1x6.ration.model.SignUpInfo
import ru.s1x6.ration.service.LoginService
import javax.servlet.http.HttpServletRequest

@CrossOrigin("*")
@RestController
class LoginController {

    @Autowired
    private lateinit var loginService: LoginService

    @PostMapping(value = ["/sign-up"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun signUp(@RequestBody info: SignUpInfo): Result {
        return loginService.signUp(info)
    }

    @PostMapping(
            path = ["/login"],
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun login(@RequestBody loginInfo: LoginInfo, request: HttpServletRequest): Result {
        return loginService.tryLogin(loginInfo, request.remoteAddr, request)
    }
}