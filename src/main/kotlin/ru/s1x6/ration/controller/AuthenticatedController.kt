package ru.s1x6.ration.controller

import org.springframework.security.core.Authentication
import ru.s1x6.ration.security.UserDetails

open class AuthenticatedController {
    protected fun getUserIdByAuthentication(authentication: Authentication): Long? {
        return if (authentication.principal is UserDetails.ExtendedUserDetails) {
            (authentication.principal as UserDetails.ExtendedUserDetails).id
        } else {
            null
        }
    }
}